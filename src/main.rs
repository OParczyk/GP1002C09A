extern crate olc_pixel_game_engine;

//This program makes use of the oneLoneCoder Pixel Game Engine by Javidx9 as a visual frontend
use crate::olc_pixel_game_engine as olc;

mod connector;

fn main() {
    let mut _connector = connector::create_connector(
        &connector::ConnectionVariant::M6809,
        (true, false, true, false),
    );

    let mut window = connector::display::create_display(_connector.interface);
    olc::start("GP1002C09A", &mut window, 320, 240, 2, 2).unwrap();
}
