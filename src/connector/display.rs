extern crate olc_pixel_game_engine;
use crate::olc_pixel_game_engine as olc;

pub mod interface;

const HIGH: bool = true;
const LOW: bool = false;

// This program will be a simulator for the Futaba GP1002C09A display module
pub struct DisplayModule {
    memory: [(u8, f32); 0x4000],
    interface: interface::Interface,
}

pub fn create_display(_interface: interface::Interface) -> DisplayModule {
    DisplayModule {
        interface: _interface,
        memory: [(0 as u8, 0 as f32); 0x4000],
    }
}

impl olc::Application for DisplayModule {
    fn on_user_create(&mut self) -> Result<(), olc::Error> {
        Ok(())
    }

    fn on_user_update(&mut self, _elapsed_time: f32) -> Result<(), olc::Error> {
        // Mirrors `olcPixelGameEngine::onUserUpdate`. Your code goes here.

        self.memory = interface::get_current_display(&mut self.interface, _elapsed_time);

        let brightness = 15 - (self.memory[0x3fff].0 >> 4);
        let pixel = olc::Pixel {
            r: 0,
            g: 16 * brightness,
            b: 0,
            a: 255,
        };

        for y in 0..240 {
            for x in 0..40 {
                let current_mem_index = x + y * 0x40;
                let cell = self.memory[current_mem_index];
                if cell.1 - _elapsed_time <= 0.0 {
                    for i in 0..8 {
                        if cell.0 & (0b0000_0001 << i) > 0 {
                            olc::draw((x * 8 + i) as i32, y as i32, pixel);
                        } else {
                            olc::draw((x * 8 + i) as i32, y as i32, olc::BLACK);
                        }
                    }
                }
            }
        }

        Ok(())
    }

    fn on_user_destroy(&mut self) -> Result<(), olc::Error> {
        // Mirrors `olcPixelGameEngine::onUserDestroy`. Your code goes here.
        Ok(())
    }
}
